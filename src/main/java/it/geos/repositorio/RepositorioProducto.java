package it.geos.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.geos.entidades.Producto;

@Repository
public interface RepositorioProducto extends JpaRepository<Producto, Long> {

	// con sql nativo
	@Query(value = "SELECT * FROM buscarPorNombre(?1)", nativeQuery = true)
	//@Query(value = "SELECT * FROM producto WHERE nombre LIKE '%'?1'%')", nativeQuery = true)
	public abstract List<Producto> listarPorNombre(String nombre);
	

}
