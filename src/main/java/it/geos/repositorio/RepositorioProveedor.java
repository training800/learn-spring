package it.geos.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.geos.entidades.Proveedor;

@Repository
public interface RepositorioProveedor extends JpaRepository<Proveedor, Long>{

}
