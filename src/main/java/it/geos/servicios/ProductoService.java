package it.geos.servicios;

import java.util.List;

import it.geos.entidades.Producto;

public interface ProductoService {

	List<Producto> obtenerProductos();

	List<Producto> obtenerPorProveedor(int idProveedor);

	Producto registar(Producto producto);

	Producto acualizar(Producto producto);

	List<Producto> findByName(String nombre);

	List<Producto> listarPorNombre(String nombre);
	
	

}
