package it.geos.servicios.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.geos.entidades.Producto;
import it.geos.repositorio.RepositorioProducto;
import it.geos.servicios.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {
	
	
	@Autowired
	private RepositorioProducto repositorio;

	@Override
	public List<Producto> obtenerProductos() {
		// TODO Auto-generated method stub
		return repositorio.findAll();
	}

	@Override
	public List<Producto> obtenerPorProveedor(int idProveedor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Producto registar(Producto producto) {
		// TODO Auto-generated method stub
		return repositorio.save(producto);
	}

	@Override
	public Producto acualizar(Producto producto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Producto> findByName(String nombre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Producto> listarPorNombre(String nombre) {
		// TODO Auto-generated method stub
		return repositorio.listarPorNombre(nombre);
	}



	



}
