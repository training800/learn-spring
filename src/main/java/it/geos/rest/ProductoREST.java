package it.geos.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.geos.entidades.Producto;
import it.geos.servicios.impl.ProductoServiceImpl;


@RestController
@RequestMapping("/productos")
public class ProductoREST {

	@Autowired
	private ProductoServiceImpl servicio;
	
	@GetMapping
	private ResponseEntity<List<Producto>> getAllProductos() {
		return ResponseEntity.ok(servicio.obtenerProductos());
	}
	
	
	@GetMapping(value = "buscar/{name}")
	private ResponseEntity<List<Producto>> listarPorNombre(@PathVariable("name") String  name) {
		return ResponseEntity.ok(servicio.listarPorNombre(name));
	}
	

	
	
	@PostMapping
	private ResponseEntity<Producto> guardarProducto(@RequestBody Producto producto) {
		try {
			Producto obj = servicio.registar(producto);
			return ResponseEntity.created(new URI("/productos/" + obj.getId())).body(obj);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
}
