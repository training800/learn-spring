create or replace function buscarPorNombre(_in_name text)
	returns setof producto as
$$
begin
	return query
	select *
	from producto
	where nombre like _in_name
	order by nombre;
end;
$$
language plpgsql volatile

select * from producto

select * from buscarPorNombre('Mon');


insert into proveedor(ciudad, nombre) values ('Salcedo','Geos It');